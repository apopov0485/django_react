import React, { Component } from "react";
import { render } from "react-dom";
import Contacts from "./department";
import DeleteRequest from "./DeleteRequest";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loaded: false,
      placeholder: "Loading"
    };
  }

  componentDidMount() {
    fetch("department")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" };
          });
        }
        return response.json();
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          };
        });
      });
  }


  render() {
    return (
        <div>
        <Contacts contacts={this.state.data} />
        <DeleteRequest />
        </div>
    )
  }
}

export default App;

const container = document.getElementById("app");
render(<App />, container);