from django.db import models

# Create your models here.
class Lead_department(models.Model):
    name = models.CharField(max_length=100)
    floor = models.IntegerField(default=0)


class Lead(models.Model):
    department = models.ForeignKey(Lead_department, related_name='dep_name', on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    age = models.IntegerField(default=0)
    salary = models.IntegerField(default=0)
    job = models.CharField(max_length=100)
    city = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)



