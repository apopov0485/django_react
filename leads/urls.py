from django.urls import path
from .views import EmployeeViewSet, DepartmentViewSet

from rest_framework.urlpatterns import format_suffix_patterns

# grace au viewset on juste a spécifier les verbes http
employee_list = EmployeeViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
employee_detail = EmployeeViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})
department_list = DepartmentViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
department_detail = DepartmentViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})
# on renseigne les différentsz pattern de nos urls
urlpatterns = format_suffix_patterns([

    path('employee/', employee_list, name='employee-list'),
    path('employee/<int:pk>/', employee_detail, name='employee-detail'),
    path('department/<int:pk>/', department_detail, name='department-detail'),
    path('department/', department_list, name='department-list')
])