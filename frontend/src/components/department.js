import React from 'react'

const Contacts = ({ contacts }) => {
    return (
            <div>

            {contacts.map((contact) => (
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title"><a href={"department/" + contact.id}>{contact.name}</a></h5>
                        <button className="btn btn-info" type='button'>
                            <a href={"department/" + contact.id}>Modifier</a>
                        </button>
                        <h6 className="card-subtitle mb-2 text-muted">{"Etage : " + contact.floor}</h6>
                        <h6 className="card-subtitle mb-2 text-muted">{"Employés : " + contact.dep_name}</h6>
                    </div>

                </div>

            ))}
        </div>
    )
};

export default Contacts
