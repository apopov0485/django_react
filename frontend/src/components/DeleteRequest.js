import React from 'react';

class DeleteRequest extends React.Component {
    constructor(props) {
        super(props);

        this.state = { 
            status: null
        };
    }

    componentDidMount() {
        // Simple DELETE request with fetch
        fetch('employee/6', { method: 'DELETE' })
            .then(() => this.setState({ status: 'Delete successful' }));
    }

    render() {
        const { status } = this.state;
        return (
            <div className="card text-center m-3">

            </div>
        );
    }
}

export default DeleteRequest ;