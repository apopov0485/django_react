from rest_framework import serializers
from .models import Lead, Lead_department


class LeadSerializer(serializers.ModelSerializer):
    '''
    On sérialise notre objet objet model en json pour la vue
    On définit les champs qui seront repris par notre sérialisation pour pouvoir les envoyés dans notre vue
    '''

    class Meta:
        model = Lead
        fields = ['department', 'first_name', 'last_name', 'age', 'salary', 'job', 'city']


class LeadDepartment(serializers.ModelSerializer):
    '''
    on refait la meme opérations pour département
    '''
    dep_name = serializers.SlugRelatedField(
        many=True,
        read_only=True,
        slug_field='last_name'
     )
    class Meta:
        model = Lead_department
        fields = ['id', 'name', 'floor', 'dep_name']
