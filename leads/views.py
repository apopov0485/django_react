from .models import Lead, Lead_department
from .serializers import LeadSerializer, LeadDepartment

from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework import renderers




class EmployeeViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    """
    queryset = Lead.objects.all()
    serializer_class = LeadSerializer

    @action(detail=True, renderer_classes=[renderers.StaticHTMLRenderer])

    def perform_create(self, serializer):
        serializer.save()

class DepartmentViewSet(viewsets.ModelViewSet):
    '''
    on refait la meme chose pour la table departement
    '''
    queryset = Lead_department.objects.all()
    serializer_class = LeadDepartment

    @action(detail=True, renderer_classes=[renderers.StaticHTMLRenderer])

    def perform_create(self, serializer):
        serializer.save()
